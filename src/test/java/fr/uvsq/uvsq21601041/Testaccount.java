package fr.uvsq.uvsq21601041;
import org.junit.Before;
import org.hamcrest.*;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.*;

import java.math.BigDecimal;


import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
public class Testaccount {
	private BigDecimal test100;
	  private BigDecimal test200;
	  private BigDecimal invalidAmount;
	  private Account account1;
	  private Account account2;

	  @Before
	  public void setup() {
	    test100 = new BigDecimal("1000");
	    test200 = new BigDecimal("2000");
	    invalidAmount = new BigDecimal("-1");
	    account1 = new Account(test100);
	    account2 = new Account(test200);
	  }

	  @Test
	  public void compare() {
		  assertThat(account1.getsolde(), is(equalTo(test100)));
	  }

	  @Test(expected = IllegalArgumentException.class)
	  public void aCreationWithANegativeAmountShouldFail() {
	    new Account(invalidAmount);
	  }

	  @Test
	  public void testcredit() {
	    account1.credit(test100);
	    assertThat(account1.getsolde(), is(equalTo(test200)));
	  }

	  @Test(expected = IllegalArgumentException.class)
	  public void aCreditWithANegativeAmountShouldFail() {
	    account1.credit(invalidAmount);
	  }

	  @Test
	  public void testdebit() {
	    account2.debit(test100);
	    assertThat(account2.getsolde(), is(equalTo(test100)));
	  }

	  @Test(expected = IllegalArgumentException.class)
	  public void testdebitfaux() {
	    account1.debit(invalidAmount);
	  }

	  @Test
	  public void testtransfer()
	  {
		  account2.transfert(test100, account1);
		  assertThat(account2.getsolde(), is(equalTo(test100)));
		  assertThat(account1.getsolde(), is(equalTo(test200)));
	  }
	

}
