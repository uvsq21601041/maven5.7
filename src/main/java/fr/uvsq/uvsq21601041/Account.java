package fr.uvsq.uvsq21601041;
import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
public class Account {
	
	private BigDecimal solde;
    Account(BigDecimal initialBalance) {
        validateAmount(initialBalance);
        solde = initialBalance;
      }
  
    public void credit(BigDecimal  money){
    	validateAmount(solde);
    	solde = solde.add(money);
        
        
    }
    public BigDecimal getsolde() {
        return solde;
      }
     
    public void debit(BigDecimal  money){
        
        if(solde.compareTo(money) < 0){
        	throw new IllegalArgumentException("Solde insuffisant, incapable de retirer des fonds");
            
        }
        solde = solde.subtract(money);
    }

   
    public void transfert(BigDecimal  money,Account targetAccount){
    	debit(money);
        targetAccount.credit(money);
    	
    	
    }
    private static void validateAmount(BigDecimal  money) {
        if (money.compareTo(ZERO) < 0) {
          throw new IllegalArgumentException("Montant invalide");
        }
      }


	}